import java.util.Scanner;

public class xo {
    static char[][] table = { { '_', '_', '_' }, { '_', '_', '_' }, { '_', '_', '_' } };
    static int row, col;
    static char player = 'x';

    // static char[] pl = {};
    public static void WelcometoXO() {
        System.out.println("Welcome to XO!!!");
    }

    public static void inputPosition() {
        Scanner kb2 = new Scanner(System.in);
        System.out.print("Player " + player + " input position : ");
        row = kb2.nextInt();
        col = kb2.nextInt();
        table[row][col] = player;
    }
    public static void switchPlayer(){
        if (player == 'x')
            player = 'o';
        else
            player = 'x';
    }

    public static void showTable() {
        for (int i = 0; i < table.length; i++) {
            for (int j = 0; j < table.length; j++) {
                System.out.print(table[i][j] + " ");
            }
            System.out.println("");
        }
    }

    static boolean checkRow() {
        for (int c = 0; c < 3; c++) {
            if (table[row][c] != player) {
                return false;
            }
        }
        return true;
    }

    static boolean checkCol() {
        for (int r = 0; r < 3; r++) {
            if (table[r][col] != player) {
                return false;
            }
        }
        return true;
    }
    static boolean checkX1(){
        if(table[0][0] == player && table[1][1] == player && table[2][2] == player){
            return true;
        }
        return false;
    }
    static boolean checkX2(){
        if(table[0][2] == player && table[1][1] == player && table[2][0] == player){
            return true;
        }
        return false;
    }
    static boolean checkWin() {
        if (checkRow())
            return true;
        if (checkCol())
            return true;
        if (checkX1() || checkX2())
            return true;
        return false;
    }
    static void showWin(){
        System.out.println(player + " win!!!");
    }
}